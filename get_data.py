from win32com.client import Dispatch

import datetime
import os


try:
    outlook = Dispatch("Outlook.Application").GetNamespace("MAPI")
    inbox = outlook.GetDefaultFolder("6")
    all_inbox = inbox.Items
    val_date = datetime.date.today()
    sub_today = 'Мониторинг касс'
    att_today = 'Мониторинг касс'
    for msg in all_inbox:
        if msg.Subject.startswith(sub_today):
            break
    for att in msg.Attachments:
        if att.FileName.startswith(att_today):
            break
    att.SaveAsFile(os.getcwd() + '\\' + str(att.FileName))
    #Создаем COM объект
    Excel = Dispatch("Excel.Application")
    #Получаем доступ к активному листу
    wb = Excel.Workbooks.Open(os.getcwd() + '\\' + str(att.FileName))
    xlOpenXMLWorkbookMacroEnabled = 20
    wb.SaveAs(os.getcwd() + '\\fn', FileFormat=xlOpenXMLWorkbookMacroEnabled, ReadOnlyRecommended=False, TextVisualLayout=True)
    wb.Close(SaveChanges=False)
    Excel.Quit()
    os.remove(os.getcwd() + '\\' + str(att.FileName))
except:
    try:
        with open('error_log.txt', 'a') as f:
            f.write(str(datetime.date.today()) + ": GET DATA ERROR")
    except:
        with open('error_log.txt', 'w') as f:
            f.write(str(datetime.date.today()) + ": GET DATA ERROR")