from win32com.client import Dispatch
import datetime
import codecs
import os


try:
    data = datetime.date.today().strftime("%d.%m.%Y") + '\n'
    outlook = Dispatch("Outlook.Application").GetNamespace("MAPI")
    olMailItem = 0x0
    obj = Dispatch("Outlook.Application")
    newMail = obj.CreateItem(olMailItem)
    newMail.To = 'Arseniy.Bartenev@x5.ru; HOT-SD@x5.ru; Roman.Dultsev@x5.ru'
    # newMail.To = 'Arseniy.Bartenev@x5.ru; M.Solovev@x5.ru; A.Markin@x5.ru'
    newMail.CC = 'Andrey.Gavrilov@x5.ru'
    sub = "Мониторинг касс ФЗ-54 " + str(datetime.date.today().strftime("%d.%m.%Y"))
    newMail.Subject = sub
    fileObj = codecs.open("expFN.txt", "r", "utf_8_sig" )
    data += fileObj.read()
    fileObj.close()
    newMail.Body = data
    newMail.Send()
    os.remove(os.getcwd() + '\\' + "fn.txt")
    os.remove(os.getcwd() + '\\' + "expFN.txt")
except:
    try:
        with open('error_log.txt', 'a') as f:
            f.write(str(datetime.date.today()) + ": SENDING DATA ERROR")
    except:
        with open('error_log.txt', 'w') as f:
            f.write(str(datetime.date.today()) + ": SENDING DATA ERROR")