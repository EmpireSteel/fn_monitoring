# coding=utf-8
import urllib2
import logging
import json
import random

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.INFO)
logger = logging.getLogger()


class ZabbixApi:

    def __init__(self, url, user, password):
        self.url = url + "/api_jsonrpc.php"
        self.auth_token = None
        self.auth(user, password)

    def auth(self, user, password):
        response = self.zabbix_api_request("user.login", {"user": user,
                                                          "password": password})
        self.auth_token = response

    def logout(self):
        self.zabbix_api_request(method="user.logout")

    def zabbix_api_request(self, method, params=None, limit=None):
        if params is None:
            params = {}
        data = {"jsonrpc": "2.0", "method": method, "params": params}
        if limit:
            data["params"]["limit"] = limit
        data["auth"] = self.auth_token
        data["id"] = 1

        logger.debug("API request: method={method}; params={params}".format(**data))
        req = urllib2.Request(url=self.url,
                              data=json.dumps(data),
                              headers={'Content-Type': 'application/json-rpc'}
                              )
        response = urllib2.urlopen(req, timeout=60 * 20)
        if response.code == 200:
            json_data = json.loads(response.read())
            if "result" in json_data:
                res = json_data["result"]
                logger.debug("API response result: %s" % (type(res)))
                return res
            elif "error" in json_data:
                logger.error("API request msg: %s" % json_data["error"])
        else:
            logger.warning("API response code != 200")
            return []

    def get_proxy_list(self):
        """Возвращает список прокси"""
        return self.zabbix_api_request(method="proxy.get")

    def create_hosts(self, hosts_list, templates=None, proxy_name=None):

        ids = []
        for host in hosts_list:
            if proxy_name:
                proxyes = self.get_proxy_list()
                if proxy_name == "random":
                    pid = random.choice(proxyes)["proxyid"]
                    host["proxy_hostid"] = pid

            if templates:
                host["templates"] = [{"templateid": tid for tid in templates}]
            id = self.zabbix_api_request(method="host.create", params=host)
            if id:
                ids.append(id["hostids"][0])

        return ids
