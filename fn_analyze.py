# coding=utf-8
import datetime
from zapi.zapi import ZabbixApi, logger


def parse_fn_csv(fn_csv_path):
    coding = "cp1251"
    separator = "\t"
    with open(fn_csv_path) as f:
        header = [h.strip() for h in f.readline().decode(coding).encode("utf8").split(separator)]
        lines = [l.decode(coding).encode("utf8") for l in f.readlines()]
        pass

    fields_pos = {}
    for i, field in enumerate(header):
        fields_pos[field] = i

    zavods = {}
    for line in lines:
        fields = line.split(separator)
        zavod_name = fields[fields_pos["Название завода"]]
        if "закр" in zavod_name:
            continue
        zavod = fields[fields_pos["Завод"]]
        fndate = fields[fields_pos["Дата действия ФН"]]
        try:
            fndate = datetime.datetime.strptime(fndate, "%d.%m.%Y").date()
        except:
            continue
        if zavod not in zavods:
            zavods[zavod] = [fndate]
        else:
            zavods[zavod].append(fndate)
    logger.info("Parsed {} zavods nums.".format(len(zavods)))
    return zavods


def analyze_zavods_dict(zdict, days=4):
    today = datetime.datetime.now().date()
    coefficient = 0.65
    ends_soon = {}
    for zavod in zdict:
        dates = zdict[zavod]
        fn_count = len(dates)
        fn_ends_soon = [d for d in dates if d - today < datetime.timedelta(days=days + 1)]
        if len(fn_ends_soon) > fn_count * coefficient:
            ends_soon[zavod] = {
                "fn_ends_soon": fn_ends_soon,
                "fn_total": fn_count,
            }
    logger.info("Found {} zavods, with ends soon FN.".format(len(ends_soon)))
    return ends_soon


def get_data_from_zabbix(ends_soon_dict):
    zapi = ZabbixApi("http://zabbix-head.x5.ru", "zabbix_api", "zabbix_api")

    disabled_hosts = []
    ends_soon_zavods = ends_soon_dict.keys()
    for zavod in ends_soon_zavods:
        #   Заменяем номера заводов на хостнеймы
        params = {"output": ["hostid", "name", "status"], "search": {"host": zavod}}
        hosts = zapi.zabbix_api_request(method="host.get", params=params, limit=1)
        if not hosts:
            logger.warning(u"Завод {} не найден в бд заббикса".format(zavod))
            # ends_soon_dict.pop(zavod)
            continue
        hostname = hosts[0]['name']
        if hosts[0]["status"] == "1":
            disabled_hosts.append(hostname)
        ends_soon_dict[hostname] = ends_soon_dict.pop(zavod)

    # Удаляем отключенные узлы
    #for dh in disabled_hosts:
	#	ends_soon_dict.pop(dh)
    # Ищем хостнеймы с уже зажженным триггером
    logger.info("{} disabled hosts: {}".format(len(disabled_hosts), ", ".join(disabled_hosts)))

    logger.info("{} total hosts with ends soon FN.".format(len(ends_soon_dict)))
    # if not ends_soon_dict:
    #     return {}
    params = {
        "output": "triggerid",
        "filter": {
            "value": 1,
            "description": "Необходима замена ФН"
        },
    }
    triggers = zapi.zabbix_api_request(method="trigger.get", params=params)
    triggers_id = [t["triggerid"] for t in triggers]
    hosts_with_triggers = zapi.zabbix_api_request("host.get", {"triggerids": triggers_id,
                                                               "output": ["hostid", "name"],
                                                               "filter": {"status": 0}})
    # Проверяем есть ли еще ошибка
    for h in hosts_with_triggers:
        if h['name'] not in ends_soon_dict:
            ends_soon_dict[h['name']] = None

    return ends_soon_dict


def write_res_to_file(ends_soon_dict):
    """ Генерация файла для zabbix_sender:"""
    result = []
    key = "fn.expire"
    for zavod, data in ends_soon_dict.items():
        s = "%s %s " % (zavod, key)
        if data:
            fn_ends_soon = data["fn_ends_soon"]
            ends_string = ", ".join(
                [d.strftime("%d.%m.%Y") + ": " + str(fn_ends_soon.count(d)) for d in set(fn_ends_soon)])
            s += u"\"Истекают %d/%d ФН (%s)\"\n" % (len(fn_ends_soon),
                                                    data["fn_total"],
                                              ends_string)
        else:
            s += "\"\"\n"
        result.append(s.encode("utf8"))
    with open("expFN.txt", 'w') as f:
        f.writelines(result)


if __name__ == "__main__":
    zavods_dict = parse_fn_csv("fn.txt")
    res = analyze_zavods_dict(zavods_dict)
    res = get_data_from_zabbix(res)
    write_res_to_file(res)
